import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Image } from 'react-native';
import { Card } from 'react-native-elements';
import { List, ListItem } from "react-native-elements";
import HTML from 'react-native-render-html';

import {
    ActivityIndicator,
    FlatList,
    TouchableOpacity
} from "react-native";
import { bold, black } from 'ansi-colors';


export default class SearchResults extends React.PureComponent {

    _keyExtractor = (item, index) => item.id + item.position;

    renderResultItem = ({item}) => {
        var searchResult;
        var searchUrl;
        if(item.title) {
            searchResult = item.title;
            searchContent = item.content;
            searchContentWithoutbTag = searchContent.replace("<b>", " ");
            searchContentWithoutTag = searchContentWithoutbTag.replace("</b>", " ");

        }
         else {
             if(item.profile && item.profile.designation) {
                searchResult = item.profile.designation;
             }
             
             if(item.profile && item.profile.name) {
                 searchContent = item.profile.name;
                 searchContentWithoutbTag = searchContent.replace("<b>", "");
                 searchContentWithoutTag = searchContentWithoutbTag.replace("</b>", "");
                 content = String.prototype.trim(searchContentWithoutTag);
             }
             
                                 
        }   
        var url = item.url;
        console.log(item)
        return (
            <Card>             
                    <Text numberOfLines={2} style={styles.title}>{(searchResult)}</Text>
                    <Text numberOfLines={2} style={styles.url}>{url}</Text> 
                    <Text numberOfLines={2} style={styles.content}>{searchContentWithoutTag}</Text>                                        
            </Card> 

        ) 
        
    }

    render() {
        return(
                <FlatList
                    data = {this.props.dataSource}
                    keyExtractor={this._keyExtractor}            
                    renderItem = {this.renderResultItem}
                />
        );
        
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    header: {
  
    },
    title: {
      fontSize: 18,
      color: 'blue',
    },
    url: {
      fontSize: 15,
      color: 'green',
      padding: 5,
      height: 30
    },
    content: {
        fontSize: 13,
        color: 'black',
        height: 35
    }
  });