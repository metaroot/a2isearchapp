import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Image} from 'react-native';
import { TextInput } from 'react-native';
import { Button } from 'react-native'
import  NavBar  from './NavBar' 
import { Keyboard } from 'react-native'
import {
    ActivityIndicator,
    FlatList,
    TouchableOpacity
} from "react-native";
import SearchResults from './SearchResults';
import { black } from 'ansi-colors';



export default class SearchPage extends Component {
 
  constructor(props) {
      super(props);
      this.state = {
        loading: true,
        dataSource: []
      };
      this.onButtonClick = this.onButtonClick.bind(this);
      this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount(){    
    this.fetchData(this.props.navigation.state.params.text);
  }

  fetchData(text){
    fetch("http://pipilika.com:7001/PipilikaA2iSearchAPI/Search?query=" + text)
    .then(response => response.json())
    .then((responseJson)=> {
        this.setState({
            loading: false,
            dataSource: responseJson.results
        });
    })
    .catch(error=>console.log(error));
  }

  onButtonClick(text) {    
    Keyboard.dismiss();
    this.setState({
      dataSource: []
    });
    this.fetchData(text);
  }

  FlatListItemSeparator = () => {
    return (
      <View style={{
        height: .5,
        width:"100%",
        backgroundColor:"rgba(0,0,0,0.5)",
      }}
      />
    );
  }

  render() {
      return (
        
        <View style={styles.container}>
          <View style= {styles.navBar}>
            <NavBar 
              onSearch={this.onButtonClick}
            /> 
          </View>          
          <View style={styles.searchResults}>
            <SearchResults dataSource={this.state.dataSource}/>
          </View>                    
        </View>
      );
  }


}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {

  },
  navBar: {
    flex: 2,
  },
  searchResults: {
    flex: 15,
  }
});
