import React, { Component } from 'react';
import { StatusBar, KeyboardAvoidingView } from 'react-native'
import { SearchBar, withTheme } from 'react-native-elements';
import { StyleSheet, Text, View } from 'react-native';
import {  Image } from 'react-native';
import { white } from 'ansi-colors';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import SearchResults from './SearchResults';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class HomePage extends Component {
  state = {
    search: '',
  }; 

  updateSearch = search => {
    this.setState({ search });
  };

  onSearchEnter() {
    const {navigate} = this.props.navigation;

   // console.log(this.state.search);
    navigate('Search', {text: this.state.search});
  }

  render() {
    
    const { search } = this.state;
    

    return (
        <KeyboardAvoidingView style={{
            flexDirection: "column",
            paddingVertical: 150,
            paddingHorizontal: 10,
            alignContent:"space-around",
            behavior: "padding"
            }}>

            <Image
                style={{width: 212,
                    height: 70,
                    justifyContent: 'center', 
                    alignSelf: 'center',
                    paddingHorizontal: 20,
                    paddingVertical:20,
                    margin: 10}}
                source={{uri: 'https://pipilika.com/img/logo.png'}}
            />
                
            <SearchBar
                style={{paddingHorizontal: 10,
                     paddingVertical: 100,
                     paddingHorizontal: 10,
                    }}
                placeholder="অনুসন্ধান করুন..."
                onChangeText={this.updateSearch}
                value={search}
                searchIcon={{ onPress: this.onSearchEnter.bind(this)}}
                lightTheme={true}
                
            />
            <View style={{ height: 60 }} />
        </KeyboardAvoidingView>
      
    );
  }
}

const styles = StyleSheet.create({
    SearchBar: {
      padding: 100,
      alignContent: "center",
      fontSize: 30,
    },
    
  });
  