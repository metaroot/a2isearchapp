import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Image} from 'react-native';
import { TextInput } from 'react-native';
import { Button } from 'react-native'
import { SearchBar, colors } from 'react-native-elements';
import { Icon } from 'react-native-elements'

import {
    ActivityIndicator,
    FlatList,
    TouchableOpacity
} from "react-native";
import { red } from 'ansi-colors';


export default class NavBar extends Component {

  constructor(props) {
    super(props)
    this.state = {
      text: '',
    }
    this.onSearchEnter = this.onSearchEnter.bind(this)
  }
  

  onSearchEnter() {
    this.props.onSearch(this.state.text);
  }

  render() {
    return (
      // Try setting `flexDirection` to `column`.
    <View style={{flex: 1, flexDirection: "column",  paddingVertical: 24}}>
            <SearchBar
                    style={{flex: 1, flexDirection: "row"}}
                    placeholder="অনুসন্ধান করুন..."
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                    searchIcon={{ onPress: this.onSearchEnter }}
                    lightTheme={true}
              
            />
      </View>   
    );
  }
};
