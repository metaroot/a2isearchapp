import {createStackNavigator, createAppContainer} from 'react-navigation';
import HomePage from './Components/HomePage';
import SearchPage from './Components/SearchPage';
import SearchResults from './Components/SearchResults';

const MainNavigator = createStackNavigator({
  Home: {screen: HomePage,
    navigationOptions: {
       header: null,
    },
  },
  Search: {screen: SearchPage,
    navigationOptions: {
      header: null,
    },
  },
});

const App = createAppContainer(MainNavigator);

export default App;